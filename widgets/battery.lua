-- Emil 'Skeen' Madsen <sovende@gmail.com>

-- Widget and layout library
local wibox = require("wibox")

local batteryWidget = {}

-- returns a string with battery info
function battery_status ()

    -- Read acpi output
    local fd = io.popen("acpi", "r")
    if not fd then
        do return "NO BAT" end
    end
    local text = fd:read("*a")
    io.close(fd)

    -- Get battery precentage
    local battery = tonumber(string.match(text, "(%d+)%%"));

    -- Figure out which icon we need
    local icon = "▸"
    if string.match(text, "Discharging") then
        icon = "▾"
    elseif string.match(text, "Charging") then
        icon = "▴"
    end

    -- Get time to discharge / charged
    local time = string.match(text, "(%d+:%d+):%d+");
    -- If we got any, let's append it
    if(time) then
        return icon .. battery .. "%" .. " / " .. time
    else
        return icon .. battery .. "%"
    end
end

-- Create a battery widget
function batteryWidget.create()
    local widget = wibox.widget.textbox();

    widget.refresh = function(self)
        widget:set_text(" " .. battery_status() .. " ");
    end

    widget:refresh();

    -- Update hook called every 20 seconds
    local widget_update_timer = timer({timeout = 20});
    widget_update_timer:connect_signal("timeout", function()
        widget:refresh();
    end)
    widget_update_timer:start();

    return widget;
end

return batteryWidget;
