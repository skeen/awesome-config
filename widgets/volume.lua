-- Emil 'Skeen' Madsen <sovende@gmail.com>

-- Widget and layout library
local wibox = require("wibox")

local volumeWidget = {}

-- returns a string with volume info
function volume_status()

    -- Read amixer output
    local fd = io.popen("amixer -c 1 sget 'Master'", "r")
    if not fd then
        do return "NO VOL" end
    end
    local text = fd:read("*a")
    io.close(fd)

    -- Figure out if we're muted
    local muted = string.match(text, "%[off%]");
    -- Return the volume %
    local volume = string.match(text, "%[(%d+)%%%]");

    if(muted) then
        return "⚫" .. volume
    else
        return "⚪" .. volume
    end
end

-- Create a volume widget
function volumeWidget.create()
    local widget = wibox.widget.textbox();

    widget.refresh = function(self)
        widget:set_text(" " .. volume_status() .. " ");
    end

    widget:refresh();

    -- Update hook called every 10 seconds
    local widget_update_timer = timer({timeout = 20});
    widget_update_timer:connect_signal("timeout", function()
        widget:refresh();
    end)
    widget_update_timer:start();

    return widget;
end

return volumeWidget;
