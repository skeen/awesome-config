local etc = {}

function etc.getHostname()
    -- Run /bin/hostname
    local f = io.popen("/bin/hostname");
    -- Capture stdout
    local hostname = f:read("*a") or "";
    -- Close the file stream
    f:close();
    -- Remove newlines
    hostname = string.gsub(hostname, "\n$", "");
    return hostname;
end

return etc;
