.SECONDARY: lua-lgi

# State about Awesome
AWESOME_PROGRAM=$(shell which awesome)
AWESOME_VERSION=$(shell awesome -v | sed -n "s/awesome v\([0-9.]*\).*/\1/gp")
AWESOME_VERSION_GOOD=$(shell expr "$(AWESOME_VERSION)" \>= 3.5.5)
# Check that Awesome if found
ifeq (, $(AWESOME_PROGRAM))
$(error Awesome not found!)
endif
# Check that awesome new enough
ifeq (0, $(AWESOME_VERSION_GOOD))
$(error Error too old Awesome version ($(AWESOME_VERSION) < 3.5.5) $(AWESOME_VERSION_GOOD))
endif

all:
