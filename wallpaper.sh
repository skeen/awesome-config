#!/bin/bash

wget "https://wiki.debian.org/DebianArt/Themes/Lines?action=AttachFile&do=get&target=Lines_2015-02-19.zip" -O Lines.zip
unzip Lines.zip
mkdir -p res
cp Lines/Desktop_wallpaper/png/wallpaper_1920x1080.png res/wallpaper.png
rm Lines.zip
rm -rf Lines
