local run = require("utils.run");
local etc = require("utils.etc");

--------------------
-- Programs below --
--------------------

-- TODO: Rename run.once to run.unless_running
-- TODO: Implement run.once (i.e. per awesome start-up)

-- Host specific programs
host_specific = {
    ["Lenovo-Laptop"] = function()
        --run.once("sshfs root@SkeenServer:/ext ~/SkeenServer");
        run.once("nm-applet");
        run.once("xscreensaver -no-splash");
        run.once("redshift -l 56.2444427:10.2411508");
    end,
    ["ballpit"] = function()
        -- Setup monitors
        run.once("xrandr --auto --output VGA2 --mode 1366x768");
        run.once("xrandr --output HDMI3 --right-of VGA2");
        run.once("xrandr --output HDMI2 --left-of VGA2");
    end,
    ["thinkpad-a22m"] = function()
        --run.once("echo " .. etc.getHostname());
        run.once("xmodmap /home/skeen/.Xmodmap");
        run.once("nm-applet");
    end
}

-- Non-host specific programs
run.once("setxkbmap dk");
run.once("guake");

-- Fire the host specifics
host_specific[etc.getHostname()]();
